const fs = require('fs');

var fetchNotes = () => {
	try {
		var notesString = fs.readFileSync('notes-data.json');
		return JSON.parse(notesString);
	} catch (e) {
		return [];
	}
};

var saveNotes = (notes) => {
	fs.writeFileSync('notes-data.json', JSON.stringify(notes));
};

module.exports = {
	addNote: (title, body) => {
		var notes = fetchNotes();
		var note = {
			title: title,
			body: body
		};
		var duplicateNotes = notes.filter((note) => {
			return note.title === title;
		});

		if (duplicateNotes.length == 0) {
			notes.push(note);
			saveNotes(notes);
			return note;
		}
	},
	getAll: () => {
		return fetchNotes();
	},
	getNote: (title) => {
		var notes = fetchNotes();
		return notes.filter((note) => {
			return note.title === title;
		})[0];
	},
	removeNote: (title) => {
		var notes = fetchNotes();
		var newNotes = notes.filter((note) => {
			return note.title !== title;
		});
		saveNotes(newNotes);

		return notes.length !== newNotes.length;
	},
	logNote: (note) => {
		if (note) {
			console.log('---')
			console.log('Title:', note.title);
			console.log('Body:', note.body);
		} else {
			console.log('Note title already exists');
		}
	}
};
	