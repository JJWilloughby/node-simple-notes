# simple-notes
Simple command line notes app written in nodejs

## Installation
- Clone repo
- Navigate to root of project and `npm install`

## Usage
- Run app with `node app.js <command> <args>`
- Available commands:
  - `add -t <title> -b <body>`
  - `list`
  - `read -t <title>`
  - `remove -t <title>`

## Purpose
This project acted as a simple introduction to node
